#!/usr/bin/env emacs --script
;; Emacs script for exporting all org files in current directory to pdf
;; using LaTeX and beamer.
;
;; Author: Sam Sinayoko, Hans Fangohr, 27/12/2015
;; Email: s.sinayoko@soton.ac.uk
;; Date: 05/10/2014

(require 'ox-latex)

;; Define an interactive function for easy testing
(defun org-beamer-export-to-pdf-directory (files)
  "Export all files to latex"
  (interactive "Export org files to tex")
  (save-excursion
    (let ((org-files-lst ))
      (dolist (org-file files)
        (message "*** Exporting file %s ***" org-file)
        (find-file org-file)
        (org-latex-export-to-latex)
        (kill-buffer)))))

(defun sa-ignore-headline (contents backend info)
  "Ignore headlines with tag `ignoreheading'."
  (when (and (org-export-derived-backend-p backend 'latex 'html 'ascii)
             (string-match "\\(\\`.*\\)ignoreheading\\(.*\n\\)"
                           (downcase contents)))
                                        ;(replace-match "\\1\\2" nil nil contents)  ;; remove only the tag ":ignoreheading:" but keep the rest of the headline
    (replace-match "" nil nil contents)        ;; remove entire headline
    ))
(add-to-list 'org-export-filter-headline-functions 'sa-ignore-headline)

(add-to-list 'org-latex-classes
    '("myreport"
      "\\documentclass[11pt]{report}"
      ;; ("\\part{%s}" . "\\part*{%s}")
      ("\\chapter{%s}" . "\\chapter*{%s}")
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))

;; Use utf8x for LaTeX export to access more unicode characters
(setq org-latex-inputenc-alist '(("utf8" . "utf8x")))
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options'(("bgcolor" "white") ("fontsize" "\\small") ("texcomments")))

;; Export all org files given on the command line
(org-beamer-export-to-pdf-directory argv)
